package com.stpl.thatssuspiciousbehavior;



import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.stpl.thatssuspiciousbehavior.R.id;
import com.stpl.thatssuspiciousbehavior.Models.Common;
import com.stpl.thatssuspiciousbehavior.Models.Constant;
import com.stpl.thatssuspiciousbehavior.Models.NetworkResponse;
import com.stpl.thatssuspiciousbehavior.Models.ReportData;

public class RegisterAccountActivity extends Activity {

	ProgressDialog progress; 
	boolean customTitleSupported;
	Common com;
	EditText firstNameText, lastNameText, emailStringText, passwordStringText, repeatPasswordStringText; 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      //vj to avoid keyboard appearance automatically
        //this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN); 
        //vj
        com = new Common();
        customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.activity_register_account);
        customTitleBar(getText(R.string.title_activity_register_account).toString());
        firstNameText = (EditText) findViewById(R.id.First_Name);
        lastNameText = (EditText) findViewById(R.id.Last_Name);
        emailStringText = (EditText) findViewById(R.id.Login_Stirng);
        passwordStringText = (EditText) findViewById(id.Password_String);
        repeatPasswordStringText = (EditText) findViewById(R.id.Repeat_Password);
        
        
        //VJ
        /*
        firstNameText.setOnFocusChangeListener(new OnFocusChangeListener()
        {
            
            public void onFocusChange(View v, boolean hasFocus) 
            {
                if (hasFocus==true)
                {
                	String s = firstNameText.getHint().toString();
                	if (s.equals("  FIRST NAME"))
                    {
                		firstNameText.setHint("");
                    }
                }
            }
        });
        lastNameText.setOnFocusChangeListener(new OnFocusChangeListener()
        {
            
            public void onFocusChange(View v, boolean hasFocus) 
            {
                if (hasFocus==true)
                {
                	String s = lastNameText.getHint().toString();
                	if (s.equals("  LAST NAME"))
                    {
                		lastNameText.setHint("");
                    }
                }
            }
        });
        emailStringText.setOnFocusChangeListener(new OnFocusChangeListener()
        {
            
            public void onFocusChange(View v, boolean hasFocus) 
            {
                if (hasFocus==true)
                {
                	String s = emailStringText.getHint().toString();
                	if (s.equals("  EMAIL"))
                    {
                		emailStringText.setHint("");
                    }
                }
            }
        });
        passwordStringText.setOnFocusChangeListener(new OnFocusChangeListener()
        {
            
            public void onFocusChange(View v, boolean hasFocus) 
            {
                if (hasFocus==true)
                {
                	String s = passwordStringText.getHint().toString();
                	if (s.equals(" PASSWORD"))
                    {
                		passwordStringText.setHint("");
                    }
                }
            }
        });
        repeatPasswordStringText.setOnFocusChangeListener(new OnFocusChangeListener()
        {
            
            public void onFocusChange(View v, boolean hasFocus) 
            {
                if (hasFocus==true)
                {
                	String s = repeatPasswordStringText.getHint().toString();
                	if (s.equals(" REPEAT PASSWORD"))
                    {
                		repeatPasswordStringText.setHint("");
                    }
                }
            }
        });
       *///VJ 
    }
    

    public void customTitleBar(String activityTitle) {
        if (customTitleSupported) {

            getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
                TextView titleActivity = (TextView) findViewById (R.id.centerText);
 
                titleActivity.setText(activityTitle);
                Button backButton = (Button) findViewById(R.id.back_button);
                //hide the button if it is not needed
                backButton.setVisibility(Button.GONE);
                
        }
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_register_account, menu);
        return false;
    }

    public void getConfirmRegistrationView(View view) {
    	String firstName = firstNameText.getText().toString();
    	String lastName = lastNameText.getText().toString();
    	final String email = emailStringText.getText().toString();
    	final String password = passwordStringText.getText().toString();
    	String repeatPassword = repeatPasswordStringText.getText().toString();
    	
    	final String signUpURL = "http://demosafetyawarenessapps.safetyawarenessapps.com/app/index.php/user/feed?email="+email+"&fname="+firstName+"&lname="+lastName+"&password="+password+"&create=1&apptoken=uscsa1221$";
    	if(com.haveNetworkConnection(this)){
        	if((firstName.length()>0)&&(lastName.length()>0)&&(email.length()>0)&&(password.length()>0)&&(repeatPassword.length()>0)){
        		
        		if(!password.equals(repeatPassword)){
    	    		com.showPopup("Both password and repeat password must match", this);
    	    		return;
    	    	}
        	}
    	    else{
    	    		com.showPopup("All Fields are required", this);
    	    		return;
    	    	}
        		//vj s
        		Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
        		Matcher m = p.matcher(email);
        		boolean matchFound = m.matches();
        		if(!matchFound){
        			com.showPopup("Not valid mail id.", this);
        			return;
        		}
        		//vj e
    	try
        {
                //create an instance of the DefaultHandler class 
                //**ALTER THIS FOR YOUR CLASS NAME**
            XMLDataHandler handler = new XMLDataHandler();
                  
            NetworkResponse responseDelegate = new NetworkResponse() {
				
				public void updateNetworkTask(String value, ArrayList<ReportData> reportDataArray, Context context, String fact) {
					
					System.out.println(value);
		            if(value.equalsIgnoreCase("OK")){
		            	Intent intent = new Intent(context, ConfirmRegistrationActivity.class);
		            	intent.putExtra("email",email);
		            	intent.putExtra("password", password);
		            	intent.putExtra("signUPURL", signUpURL);
		                startActivity(intent);
		            }else{
		            	 com.showPopup(value, context);
		            }
					
				}
			};
                handler.getData(signUpURL,this, responseDelegate);
            
          }
        catch(Exception pce) {
        	//Log.e("AndroidTestsActivity", "PCE "+pce.getMessage());
        }
    	}else{
    		com.showPopup(Constant.connectionMessage, this);
    	}
    	
    }
    
    public void getTermsActivity(View view){
    	Intent intent = new Intent(this, WebViewActivity.class);
        intent.putExtra(Common.URL_KEY, "http://demosafetyawarenessapps.safetyawarenessapps.com/plist/terms.html");
        intent.putExtra("title","TERMS");
        startActivity(intent);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showPopup(String message){
    	// prepare the alert box                
        AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
         
        // set the message to display
        alertbox.setMessage(message);
         
        // add a neutral button to the alert box and assign a click listener
        alertbox.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
             
            // click listener on the alert box
            public void onClick(DialogInterface arg0, int arg1) {
                // the button was clicked
                
            }
        });
         
        // show it
        alertbox.show();
    }
}