

package com.stpl.thatssuspiciousbehavior;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
import com.stpl.thatssuspiciousbehavior.Models.Common;
import com.stpl.thatssuspiciousbehavior.Models.Constant;
import com.stpl.thatssuspiciousbehavior.Models.MyLocation;
import com.stpl.thatssuspiciousbehavior.Models.NetworkResponse;
import com.stpl.thatssuspiciousbehavior.Models.ReportData;

public class ReportMapActivity extends MapActivity {

	boolean customTitleSupported;
	Dialog myDialog;

	//ProgressDialog progress; 
	MapView mapView;
	Common com;
	MyLocation loc;
	String finalURL;
	public ArrayList<ReportData> tempReportDataArray;
	//public ArrayList<ReportData> filterReportDataArray;
	
	int aggressiveReporting, vandalReporting, creepyReporting,	fightReporting, theftReporting;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        com = new Common();
        customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        
        setContentView(R.layout.activity_report_map);
        customTitleBar(getText(R.string.title_activity_report_map_view).toString());
        
        mapView = (MapView) findViewById(R.id.googleMapview);
        mapView.setBuiltInZoomControls(true);
        MapController mapController = mapView.getController();
        GeoPoint myLocation = new GeoPoint((int) (com.getCurrentLocation(this).getLattitude()* 1E6),(int)(com.getCurrentLocation(this).getLongitude() * 1E6));
        mapController.animateTo(myLocation);
        mapController.setZoom(15);
        
        
        
        com = new Common();
        tempReportDataArray = new ArrayList<ReportData>();
        loc = com.getCurrentLocation(this);
        //String finalURL = Constant.serverUrl+"latitude="+loc.getLattitude()+"&longitude="+loc.getLongitude();
        finalURL = Constant.serverUrl+"latitude="+loc.getLattitude()+"&longitude="+loc.getLongitude();
        float dateRange = this.getDayRangeFromMemory();
        String simpleMapUrl = com.makeReportUrl(Constant.reports_by_default,finalURL,dateRange);
        this.getReportResponse(simpleMapUrl);        
        
        
        Button viewButton = (Button) findViewById(R.id.select_view_Button);
        Button timeFrameButton = (Button) findViewById(R.id.button_timeframe);
        Button tableViewButton = (Button) findViewById(R.id.table_view_button);
        
        // click on viewButton dialog will appear
        viewButton.setOnClickListener(new OnClickListener() {        	 
        public void onClick(View v) {
	        	myDialog = new Dialog(ReportMapActivity.this);
	        	myDialog.setContentView(R.layout.pop_up_layout);
	        	myDialog.setTitle("Select View Type");
	        	//myDialog.setCancelable(true);
                Button mapViewButton = (Button) myDialog.findViewById(R.id.map_view_button);
                Button satelliteViewButton = (Button) myDialog.findViewById(R.id.satellite_view);
                Button hybridViewButton = (Button) myDialog.findViewById(R.id.hybrid_view);
                	
                mapViewButton.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                	myDialog.dismiss();
                	mapView.setSatellite(false);
                    }
                });
 
                satelliteViewButton.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                    	myDialog.dismiss();
                    	mapView.invalidate();
                    	mapView.setSatellite(true);
                        }
                    });
                
                hybridViewButton.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                    	myDialog.dismiss();
                    	mapView.setSatellite(true);
                        }
                    });
                
                myDialog.show();
            }
        });
        
        // click on time frame button dialog will appear
        timeFrameButton.setOnClickListener(new OnClickListener() {        	 
            public void onClick(View v) {
            	finalURL = Constant.serverUrl+"latitude="+loc.getLattitude()+"&longitude="+loc.getLongitude();;
    	        	myDialog = new Dialog(ReportMapActivity.this);
    	        	myDialog.setContentView(R.layout.time_frame_layout);
    	        	myDialog.setTitle("Select Time");
    	        	//myDialog.setCancelable(true);
                    Button last12Hours = (Button) myDialog.findViewById(R.id.last_12_hours);
                    Button last24Hours = (Button) myDialog.findViewById(R.id.last_24_hours);
                    Button last2Days = (Button) myDialog.findViewById(R.id.last_2_days);
                    Button lastWeek = (Button) myDialog.findViewById(R.id.last_week);
                    Button lastMonth = (Button) myDialog.findViewById(R.id.last_month);

                    
                	last12Hours.setOnClickListener(new OnClickListener() {
                    	ReportMapActivity reportMapActivity = ReportMapActivity.this;
                    public void onClick(View v) {
                    	String timeFrameURL = com.makeReportUrl(Constant.reports_by_time_range,finalURL,0.5f);
                    	reportMapActivity.tempReportDataArray = new ArrayList<ReportData>();
                        reportMapActivity.getReportResponse(timeFrameURL);
                        reportMapActivity.saveDayRangeInMemory(0.5f);
                    	myDialog.dismiss();
                        }
                    });
     
                    last24Hours.setOnClickListener(new OnClickListener() {

                    	ReportMapActivity reportMapActivity = ReportMapActivity.this;
                        public void onClick(View v) {
                        	String timeFrameURL = com.makeReportUrl(Constant.reports_by_time_range,finalURL,1);
                        	reportMapActivity.tempReportDataArray = new ArrayList<ReportData>();
                            reportMapActivity.getReportResponse(timeFrameURL);
                            reportMapActivity.saveDayRangeInMemory(1);
                        	myDialog.dismiss();
                            }
                        });
                    
                    last2Days.setOnClickListener(new OnClickListener() {
                    	ReportMapActivity reportMapActivity = ReportMapActivity.this;
                        public void onClick(View v) {
                        	String timeFrameURL = com.makeReportUrl(Constant.reports_by_time_range,finalURL,2);
                        	reportMapActivity.tempReportDataArray = new ArrayList<ReportData>();
                            reportMapActivity.getReportResponse(timeFrameURL);
                            reportMapActivity.saveDayRangeInMemory(2);
                        	myDialog.dismiss();
                            }
                        });
                    
                    lastWeek.setOnClickListener(new OnClickListener() {
                    	ReportMapActivity reportMapActivity = ReportMapActivity.this;
                        public void onClick(View v) {
                        	String timeFrameURL = com.makeReportUrl(Constant.reports_by_time_range,finalURL,7);
                        	reportMapActivity.tempReportDataArray = new ArrayList<ReportData>();
                            reportMapActivity.getReportResponse(timeFrameURL);
                            reportMapActivity.saveDayRangeInMemory(7);
                        	myDialog.dismiss();
                            }
                        });
                    
                    lastMonth.setOnClickListener(new OnClickListener() {
                    	ReportMapActivity reportMapActivity = ReportMapActivity.this;
                        public void onClick(View v) {
                        	String timeFrameURL = com.makeReportUrl(Constant.reports_by_time_range,finalURL,30);
                        	reportMapActivity.tempReportDataArray = new ArrayList<ReportData>();
                            reportMapActivity.getReportResponse(timeFrameURL);
                            reportMapActivity.saveDayRangeInMemory(30);
                        	myDialog.dismiss();
                            }
                        });
                    myDialog.show();
                    
                }
            //String urlSegment = Constant.serverUrl+"latitude="+loc.getLattitude()+"&longitude="+loc.getLongitude();
           
            
        });
        
     // click on tableView button dialog will appear
        tableViewButton.setOnClickListener(new OnClickListener() {        	 
            public void onClick(View v) {
            	myDialog = new Dialog(ReportMapActivity.this);
	        	myDialog.setContentView(R.layout.report_table_view);
	        	myDialog.setTitle("Select View Type");
	        	//myDialog.setCancelable(true);
                Button aggressiveButton = (Button) myDialog.findViewById(R.id.aggressive_button);
                Button vandalButton = (Button) myDialog.findViewById(R.id.vandal_button);
                Button creepyButton = (Button) myDialog.findViewById(R.id.creepy_button);
                Button fightButton = (Button) myDialog.findViewById(R.id.fight_button);
                Button theftButton = (Button) myDialog.findViewById(R.id.theft_button);

                aggressiveButton.setText(aggressiveReporting +" incident reported");
                vandalButton.setText(vandalReporting+" incident reported");
                creepyButton.setText(creepyReporting+" incident reported");
                theftButton.setText(theftReporting+" incident reported");
                fightButton.setText(fightReporting+" incident reported");
                
                
            	aggressiveButton.setOnClickListener(new OnClickListener() {
            		ReportMapActivity reportMapActivity = ReportMapActivity.this;
                public void onClick(View v) {
                	String stringURL = Constant.serverUrl+"latitude="+loc.getLattitude()+"&longitude="+loc.getLongitude()+"&incident_area=3&type="+"Aggressive Begging";
                	float dayRange = reportMapActivity.getDayRangeFromMemory();
                	String tableViewURL = com.makeReportUrl(Constant.reports_by_incident,stringURL,dayRange);
                	reportMapActivity.getReportResponse(tableViewURL);
                	myDialog.dismiss();
                    }
                });
 
                vandalButton.setOnClickListener(new OnClickListener() {

                	ReportMapActivity reportMapActivity = ReportMapActivity.this;
                    public void onClick(View v) {
                    	String stringURL = Constant.serverUrl+"latitude="+loc.getLattitude()+"&longitude="+loc.getLongitude()+"&incident_area=3&type="+"Vandal";
                    	float dayRange = reportMapActivity.getDayRangeFromMemory();
                    	String tableViewURL = com.makeReportUrl(Constant.reports_by_incident,stringURL,dayRange);
                        reportMapActivity.getReportResponse(tableViewURL);
                    	myDialog.dismiss();
                        }
                    });
                
                creepyButton.setOnClickListener(new OnClickListener() {
                	ReportMapActivity reportMapActivity = ReportMapActivity.this;
                    public void onClick(View v) {
                    	String stringURL = Constant.serverUrl+"latitude="+loc.getLattitude()+"&longitude="+loc.getLongitude()+"&incident_area=3&type="+"Creepy Person";
                    	float dayRange = reportMapActivity.getDayRangeFromMemory();
                    	String tableViewURL = com.makeReportUrl(Constant.reports_by_incident,stringURL,dayRange);
                        reportMapActivity.getReportResponse(tableViewURL);
                    	myDialog.dismiss();
                        }
                    });
                
                fightButton.setOnClickListener(new OnClickListener() {
                	ReportMapActivity reportMapActivity = ReportMapActivity.this;
                    public void onClick(View v) {
                    	String stringURL = Constant.serverUrl+"latitude="+loc.getLattitude()+"&longitude="+loc.getLongitude()+"&incident_area=3&type="+"Fight";
                    	float dayRange = reportMapActivity.getDayRangeFromMemory();
                    	String tableViewURL = com.makeReportUrl(Constant.reports_by_incident,stringURL,dayRange);
                        reportMapActivity.getReportResponse(tableViewURL);
                    	myDialog.dismiss();
                        }
                    });
                
                theftButton.setOnClickListener(new OnClickListener() {
                	ReportMapActivity reportMapActivity = ReportMapActivity.this;
                    public void onClick(View v) {
                    	String stringURL = Constant.serverUrl+"latitude="+loc.getLattitude()+"&longitude="+loc.getLongitude()+"&incident_area=3&type="+"Possible Theft";
                    	float dayRange = reportMapActivity.getDayRangeFromMemory();
                    	String tableViewURL = com.makeReportUrl(Constant.reports_by_incident,stringURL,dayRange);
                        reportMapActivity.getReportResponse(tableViewURL);
                    	myDialog.dismiss();
                        }
                    });
                myDialog.show();
           
            }
        });
    }
      
    public void saveDayRangeInMemory(float days){
    	// We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        SharedPreferences settings = getSharedPreferences("EmailStorage", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putFloat("dayRange",days);

        // Commit the edits!
        editor.commit();
    }
   
    public float getDayRangeFromMemory(){
    	//new FetchTask(this).execute();
    	// Restore preferences
        SharedPreferences settings = getSharedPreferences("EmailStorage", 0);
        float dayRange = settings.getFloat("dayRange", 0);
        return dayRange;
    }
   /* protected void onResume() {
    	super.onResume();
    // when our activity resumes, we want to register for location updates
    	myLocationOverlay.enableMyLocation();
    }
   
     
    
    @Override
   
    protected void onPause() {
    	super.onPause();
    	// when our activity pauses, we want to remove listening for location updates
    	myLocationOverlay.disableMyLocation();
    }*/

   
    public void showReportsLocation(ArrayList<ReportData> reportDataArray){
    	//mapView = (MapView) findViewById(R.id.googleMapview);
    	
    	if((tempReportDataArray.size()) == 0){
    		tempReportDataArray = reportDataArray;
    		this.updateReportCount(reportDataArray);
    	}
    	//if((reportDataArray.size())>0){
    	MapController mapController = mapView.getController();
        mapView.setBuiltInZoomControls(true);
        List<Overlay> mapOverlays = mapView.getOverlays();
        if(mapOverlays.size()>0){
            mapOverlays.clear();
        }
        if((reportDataArray.size())>0){
        //ReportMapActivity reportMapActivity = ReportMapActivity.this;
        Drawable drawableAggressive = this.getResources().getDrawable(R.drawable.beggar);
        Drawable drawableVandal = this.getResources().getDrawable(R.drawable.vandal);
        Drawable drawableCreepy = this.getResources().getDrawable(R.drawable.creepy);
        Drawable drawableFight = this.getResources().getDrawable(R.drawable.dispute);
        Drawable drawableTheft = this.getResources().getDrawable(R.drawable.possible_theft);
        
        ItemizedOverlayActivity aggressiveItemizedoverlay = new ItemizedOverlayActivity(drawableAggressive, this);
        ItemizedOverlayActivity vandalItemizedoverlay = new ItemizedOverlayActivity(drawableVandal, this);
        ItemizedOverlayActivity creepyItemizedoverlay = new ItemizedOverlayActivity(drawableCreepy, this);
        ItemizedOverlayActivity fightItemizedoverlay = new ItemizedOverlayActivity(drawableFight,this);
        ItemizedOverlayActivity theftItemizedoverlay = new ItemizedOverlayActivity(drawableTheft, this);
        
        
        for(int i=0; i<reportDataArray.size(); i++){
        	ReportData reportData = reportDataArray.get(i);
        	GeoPoint myLocation = new GeoPoint((int) (reportData.getLocation().getLattitude()
            	* 1E6),(int)(reportData.getLocation().getLongitude()* 1E6));
        	
        	OverlayItem overlayItem = new OverlayItem(myLocation, reportData.getReportType(), reportData.getReportDate());
        		
        	if(reportData.getReportType().equalsIgnoreCase("Aggressive Begging")){
        		aggressiveItemizedoverlay.addOverlay(overlayItem);
        	}
        	else if(reportData.getReportType().equalsIgnoreCase("VANDAL")){
        		//aggressiveItemizedoverlay.addOverlay(overlayItem);
        		vandalItemizedoverlay.addOverlay(overlayItem);
        	}
        	else if(reportData.getReportType().equalsIgnoreCase("CREEPY PERSON")){
        		creepyItemizedoverlay.addOverlay(overlayItem);
        		//aggressiveItemizedoverlay.addOverlay(overlayItem);
        	}
        	else if(reportData.getReportType().equalsIgnoreCase("FIGHT")){
        		fightItemizedoverlay.addOverlay(overlayItem);
        		//aggressiveItemizedoverlay.addOverlay(overlayItem);
        	}
        	else if(reportData.getReportType().equalsIgnoreCase("POSSIBLE THEFT")){
        		theftItemizedoverlay.addOverlay(overlayItem);
        		//aggressiveItemizedoverlay.addOverlay(overlayItem);
        	}

        	 int j1 = aggressiveItemizedoverlay.size();
       	     //int j2 = vandalItemizedoverlay.size();
       	     //int j3 = creepyItemizedoverlay.size();
       	    // int j4 = fightItemizedoverlay.size();
       	    // int j5 = theftItemizedoverlay.size();
       	    
    	    System.out.println("size of aggreesive overlay:"+j1);
    	   // System.out.println("size of vandal overlay:"+j2);
    	   // System.out.println("size of creepy overlay:"+j3);
    	   // System.out.println("size of  fight overlay:"+j4);
    	   // System.out.println("size of theft overlay:"+j5);
    	  }
       
       if(aggressiveItemizedoverlay.size()>0){
        mapOverlays.add(aggressiveItemizedoverlay);
       }
       if(vandalItemizedoverlay.size()>0){
        mapOverlays.add(vandalItemizedoverlay);
       }
       if(creepyItemizedoverlay.size()>0){
        mapOverlays.add(creepyItemizedoverlay);
       }
       if(fightItemizedoverlay.size()>0){
        mapOverlays.add(fightItemizedoverlay);
       }
       if(theftItemizedoverlay.size()>0){
        mapOverlays.add(theftItemizedoverlay);
       }
       
      }
       GeoPoint myLocation = new GeoPoint((int) (com.getCurrentLocation(this).getLattitude()* 1E6),(int)(com.getCurrentLocation(this).getLongitude() * 1E6));
       mapController.animateTo(myLocation);
       mapController.setZoom(15);

       mapView.invalidate();
    	//mapController.animateTo(myLocation);
    	
    	//} 
    }
    
    public void updateReportCount(ArrayList<ReportData> reportArray){
    	
    	aggressiveReporting = 0;
    	vandalReporting = 0;
        fightReporting = 0;
        creepyReporting = 0;
        theftReporting = 0;
    	for(int i=0; i<reportArray.size(); i++){
    		
        	ReportData reportData = reportArray.get(i);
        	if(reportData.getReportType().equalsIgnoreCase("Aggressive Begging")){
        		aggressiveReporting++;
        	}
        	else if(reportData.getReportType().equalsIgnoreCase("VANDAL")){
        		vandalReporting++;
        	}
        	else if(reportData.getReportType().equalsIgnoreCase("CREEPY PERSON")){
        		creepyReporting++;
        	}
        	else if(reportData.getReportType().equalsIgnoreCase("FIGHT")){
        		fightReporting++;
        	}
        	else if(reportData.getReportType().equalsIgnoreCase("POSSIBLE THEFT")){
        		theftReporting++;
        	}
    	}
    	
    }
    
    
    
    public void getReportResponse(String url){
    	if(com.haveNetworkConnection(this)){
            try
            {   
            	XMLDataHandler handler = new XMLDataHandler();
                NetworkResponse responseDelegate = new NetworkResponse() {
                public void updateNetworkTask(String value, ArrayList<ReportData> reportDataArray, Context context, String fact) {
                	System.out.println(value);
                	System.out.println("count"+reportDataArray.size());
                	//if(reportDataArray.size()>0){
    	            ((ReportMapActivity) context).showReportsLocation(reportDataArray);
                	//}
    	        }
            };
                handler.getData(url,this, responseDelegate);
            }
            catch(Exception exception) {
            	Log.e("AndroidTestsActivity", "exception "+exception.getMessage());
            }
        	}else{
        		com.showPopup(Constant.connectionMessage, this);
        	}
    }
    
    public void setMyPosition(View view){
    	
    	//mapView = (MapView) findViewById(R.id.googleMapview);
    	MapController mapController = mapView.getController();
        mapView.setBuiltInZoomControls(true);
    	//List<Overlay> mapOverlays = mapView.getOverlays();
        //Drawable drawable = this.getResources().getDrawable(R.drawable.dispute);
        //ItemizedOverlayActivity itemizedoverlay = new ItemizedOverlayActivity(drawable, this);
        //GeoPoint myLocation = new GeoPoint((int) (22.69000* 1E6),(int)(75.89212 * 1E6));
        GeoPoint myLocation = new GeoPoint((int) (com.getCurrentLocation(this).getLattitude()* 1E6),(int)(com.getCurrentLocation(this).getLongitude() * 1E6));
        //OverlayItem overlayItem = new OverlayItem(myLocation, "Reported In TSB", "");
        //itemizedoverlay.addOverlay(overlayItem);
        //itemizedoverlay.addOverlay(overlayitem2);
        //mapOverlays.add(itemizedoverlay);
        mapController.animateTo(myLocation);
        mapController.setZoom(15);
        mapView.invalidate();
    }
    
    public void customTitleBar(String activityTitle) {
        if (customTitleSupported) {

            getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
                TextView titleActivity = (TextView) findViewById (R.id.centerText);
 
                titleActivity.setText(activityTitle);
 
                Button termsButton;
                termsButton = (Button) findViewById(R.id.terms_button);
                //hide the progress bar if it is not needed
                termsButton.setVisibility(Button.GONE);
                Button backButton = (Button) findViewById(R.id.back_button);
                //hide the button if it is not needed
                backButton.setVisibility(Button.GONE);
        }
    }
    
    @Override
    protected boolean isRouteDisplayed() {
        return false;
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_report_map, menu);
        return false;
    }
    
    public void getTableView(View view){
    	Intent intent = new Intent(this, ReportTableViewActivity.class);
        startActivity(intent);
    }
  
    
   
}
