package com.stpl.thatssuspiciousbehavior;


import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;


public class FeatureActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feature);
       TableLayout tableLayout = (TableLayout)findViewById(R.id.my_table_view);
    		   for(int i=0;i<3;i++){
    			    // create a new TableRow
    			    TableRow row = new TableRow(this);
    			    // create a new TextView for showing xml data
    			    TextView t = new TextView(this);
    			    // set the text to "text xx"
    			    t.setText( "Feature"+i);
    			    // add the TextView  to the new TableRow
    			    row.addView(t);
    			    // add the TableRow to the TableLayout
    			    tableLayout.addView(row, new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)); 
    			}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_feature, menu);
        return true;
    }
}
