package com.stpl.thatssuspiciousbehavior;



import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.stpl.thatssuspiciousbehavior.Models.Common;
import com.stpl.thatssuspiciousbehavior.Models.Constant;

public class OptionsActivity extends Activity {
	
    boolean customTitleSupported;
    Button loginLogoutButton;
    Common com;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        com =new Common();
        
        customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.activity_options);
        customTitleBar(getText(R.string.title_activity_options).toString());
        loginLogoutButton = (Button) findViewById(R.id.login_logout_button);
        // Restore preferences
        SharedPreferences settings = getSharedPreferences("EmailStorage", 0);
        String savedEmail = settings.getString("emailString", "");
        System.out.println("email in default"+savedEmail);
        if(savedEmail != ""){
        	loginLogoutButton.setText("LOGOUT");
        }
        else{
        	loginLogoutButton.setText("LOGIN");
        }
     	
    }

    //vj calling on nuber
    public void callingOnNumber(View view){
    	com.callingProcess(this);
    }
    //vj
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_options, menu);
        
        return false;
    }

    public void getLoginView(View view) {
    	
    	String buttonText = loginLogoutButton.getText().toString();
    	if(buttonText.equals("LOGIN")){
    		Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
    	}
    	else if(buttonText.equals("LOGOUT")){
    		Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            
            // We need an Editor object to make preference changes.
            // All objects are from android.context.Context
            SharedPreferences settings = getSharedPreferences("EmailStorage", 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString("emailString", "");

            // Commit the edits!
            editor.commit();
            
            //loginLogoutButton.setText("LOGIN");
    	}
    	
    }
    
    public void customTitleBar(String activityTitle) {
        if (customTitleSupported) {

            getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
                TextView titleActivity = (TextView) findViewById (R.id.centerText);
 
                titleActivity.setText(activityTitle);
 
                Button termsButton;
                termsButton = (Button) findViewById(R.id.terms_button);
                //hide the Button if it is not needed
                termsButton.setVisibility(Button.GONE);
                Button backButton = (Button) findViewById(R.id.back_button);
                //hide the button if it is not needed
                backButton.setVisibility(Button.GONE);
        }
    }
    
    public void getBackView(View view){
    	Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
    
    
    public void getAboutUsView(View view){
    	if(com.haveNetworkConnection(this)){
            Intent intent = new Intent(this, WebViewActivity.class);
            intent.putExtra(Common.URL_KEY, "http://demosafetyawarenessapps.safetyawarenessapps.com/plist/aboutus.html");
            intent.putExtra("title","HELP");
            startActivity(intent);
        	}
    	else{
    		com.showPopup(Constant.connectionMessage, this);
    	}
    }
    //vj temp testing
    public void testIt(View view){
    	
            Intent intent = new Intent(this, FeatureActivity.class);
            startActivity(intent);
        	
    }
  //vj temp testing
    public void getTermsOfUseView(View view){
    	if(com.haveNetworkConnection(this)){
            Intent intent = new Intent(this, WebViewActivity.class);
            intent.putExtra(Common.URL_KEY, "http://demosafetyawarenessapps.safetyawarenessapps.com/plist/terms.html");
            intent.putExtra("title","TERMS");
            startActivity(intent);
        	}
    	else{
    		com.showPopup(Constant.connectionMessage, this);
    	}
    }
    
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
