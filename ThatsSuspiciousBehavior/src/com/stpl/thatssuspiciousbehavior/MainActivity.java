package com.stpl.thatssuspiciousbehavior;

import com.stpl.thatssuspiciousbehavior.Models.Common;
import com.stpl.thatssuspiciousbehavior.Models.FetchTask;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;


public class MainActivity extends Activity {

	Common com;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        com = new Common();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        
        return false;
    }
    public void getOptionViews(View view) {
    	Intent intent = new Intent(this, OptionsActivity.class);
        startActivity(intent);
    }
     
    public void getReportView(View view){
    	//new FetchTask(this).execute();
    	// Restore preferences
        SharedPreferences settings = getSharedPreferences("EmailStorage", 0);
        String savedEmail = settings.getString("emailString", "");
        System.out.println("email in default"+savedEmail);
        if(savedEmail != ""){
        	Intent intent = new Intent(this, ReportActivity.class);
            startActivity(intent);
        }
        else{
        	Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
    }
    
    public void showReportMapView(View view){
    	new FetchTask(this).execute();
    	//Common com = new Common(); 
    	//MyLocation loc = com.getCurrentLocation(this);
    	Intent intent = new Intent(this, ReportMapActivity.class);
    	//intent.putExtra("longitude",loc.getLongitude());
    	//intent.putExtra("latitude", loc.getLattitude());
        startActivity(intent);
    }
    /*public void callingOnNumber(View view){
    	try{
        	Intent callIntent = new Intent(Intent.ACTION_CALL);
        	callIntent.setData(Uri.parse("tel:911"));
        	startActivity(callIntent);
        	}
    	catch (Exception e) {
            e.printStackTrace();
          }
    }*/
    
    public void callingOnNumber(View view){
    	/*AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setMessage("Are you sure you want to call?")
    	       .setCancelable(false)
    	       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
    	           public void onClick(DialogInterface dialog, int id) {
    	        	   try{
    	               	Intent callIntent = new Intent(Intent.ACTION_CALL);
    	               	callIntent.setData(Uri.parse("tel:911"));
    	               	startActivity(callIntent);
    	               	}
    	           	catch (Exception e) {
    	                   e.printStackTrace();
    	                 }
    	           }
    	       })
    	       .setNegativeButton("No", new DialogInterface.OnClickListener() {
    	           public void onClick(DialogInterface dialog, int id) {
    	                dialog.cancel();
    	           }
    	       });
    	
    	AlertDialog alert = builder.create();
    	alert.show();*/
    	com.callingProcess(this);
    }
    
    
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK ) {
        	this.onBackPressed();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
    
    public void onBackPressed() {
    	Intent intent = new Intent();
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);
        return;
        }
}

