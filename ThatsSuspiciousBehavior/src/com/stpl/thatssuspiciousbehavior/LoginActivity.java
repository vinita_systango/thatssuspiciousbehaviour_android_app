
package com.stpl.thatssuspiciousbehavior;


import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.stpl.thatssuspiciousbehavior.Models.Common;
import com.stpl.thatssuspiciousbehavior.Models.Constant;
import com.stpl.thatssuspiciousbehavior.Models.FetchTask;
import com.stpl.thatssuspiciousbehavior.Models.NetworkResponse;
import com.stpl.thatssuspiciousbehavior.Models.ReportData;

public class LoginActivity extends Activity {

	ProgressDialog progress; 

	//download file

	
	private EditText emailText;
    private EditText passwordText;
    boolean customTitleSupported;
    public Common com;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //vj to avoid keyboard appearance automatically
        //this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN); 
        //vj
        com = new Common();
        customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.activity_login);
        customTitleBar(getText(R.string.title_activity_login).toString());
        emailText = (EditText) findViewById(R.id.email_text);
        passwordText = (EditText) findViewById(R.id.password_text);
        
        /*
      
        emailText.setOnFocusChangeListener(new OnFocusChangeListener()
        {
            
            public void onFocusChange(View v, boolean hasFocus) 
            {
                if (hasFocus==true)
                {
                	String s = emailText.getHint().toString();
                	if (s.equals("  EMAIL"))
                    {
                		emailText.setHint("");
                    }
                }
            }
        }); 
        passwordText.setOnFocusChangeListener(new OnFocusChangeListener()
        {
            
            public void onFocusChange(View v, boolean hasFocus) 
            {
                if (hasFocus==true)
                {
                	String s = passwordText.getHint().toString();
                	if (s.equals(" PASSWORD"))
                    {
                		passwordText.setHint("");
                    }
                }
            }
        });
        
        */
        
       
    }

    public void customTitleBar(String activityTitle) {
        if (customTitleSupported) {

            getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
                TextView titleActivity = (TextView) findViewById (R.id.centerText);
                titleActivity.setText(activityTitle);
                Button backButton = (Button) findViewById(R.id.back_button);
                //hide the button if it is not needed
                backButton.setVisibility(Button.GONE);
                
        }
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_login, menu);
        return false;
    }

    public void getRegisterLoginView(View view) {
    	Intent intent = new Intent(this, RegisterAccountActivity.class);
        startActivity(intent);
    }
    public void login(View view) throws XmlPullParserException, IOException  {
    	//InputStream stream = null;
    	//progress = ProgressDialog.show(this,"","Loading. Please wait...",true);
    	final String email = emailText.getText().toString();
    	String password = passwordText.getText().toString();
    	//vj s
    	if((email.length() == 0))
    	{
    		com.showPopup("Email id required.", this);
    		return;
    	}
    	else if((password.length() == 0)){
    		com.showPopup("Password required.", this);
    		return;
    	}
    	else
    	{
    		/*
    		 public final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
          "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
          "\\@" +
          "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
          "(" +
          "\\." +
          "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
          ")+"
           );
    		 */
    		
    		Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
    		Matcher m = p.matcher(email);
    		boolean matchFound = m.matches();
    		if(!matchFound){
    			com.showPopup("Not valid Email.", this);
    			return;
    		}
    	}
    	//vj e
    	String loginURL = "http://demosafetyawarenessapps.safetyawarenessapps.com/app/index.php/user/feed?email="+email+"&password="+password+"&create=0&apptoken=uscsa1221$";
    	if(com.haveNetworkConnection(this)){
    		//progress = ProgressDialog.show(this,"","Loading. Please wait...",true);
    	try
        {
                //create an instance of the DefaultHandler class 
                //**ALTER THIS FOR YOUR CLASS NAME**
            final XMLDataHandler handler = new XMLDataHandler();
            NetworkResponse responseDelegate = new NetworkResponse() {
				
				public void updateNetworkTask(String value,ArrayList<ReportData> reportDataArray, Context context, String fact) {
					// TODO Auto-generated method stub
					System.out.println(value);
					if(value.equalsIgnoreCase("OK")){
		            	
		            	// We need an Editor object to make preference changes.
		                // All objects are from android.context.Context
		                SharedPreferences settings = getSharedPreferences("EmailStorage", 0);
		                SharedPreferences.Editor editor = settings.edit();
		                editor.putString("emailString", email);

		                // Commit the edits!
		                editor.commit();
		                
		            	 Intent intent = new Intent(context, ReportActivity.class);
		                 startActivity(intent);
		            }else{
		            	 com.showPopup(value,context);
		            }
				}
			};
                //get the string list by calling the public method
            handler.getData(loginURL,this, responseDelegate);
            //System.out.println("statusValue"+handler.statusString);
            /*if(handler.statusString.equalsIgnoreCase("OK")){
            	
            	// We need an Editor object to make preference changes.
                // All objects are from android.context.Context
                SharedPreferences settings = getSharedPreferences("EmailStorage", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("emailString", email);

                // Commit the edits!
                editor.commit();
                
            	 Intent intent = new Intent(this, ReportActivity.class);
                 startActivity(intent);
            }else{
            	 com.showPopup(handler.statusString,this);
            }
            //progress.cancel();*/
        }
        catch(Exception exception) {
        	//Log.e("AndroidTestsActivity", "exception "+exception.getMessage());
        }
    	}
    	else{
    		com.showPopup(Constant.connectionMessage, this);
    	}
    	
    }
        
    
       
    
    public void getTermsActivity(View view){
    	Intent intent = new Intent(this, WebViewActivity.class);
        intent.putExtra(Common.URL_KEY, "http://demosafetyawarenessapps.safetyawarenessapps.com/plist/terms.html");
        intent.putExtra("title","TERMS");
        startActivity(intent);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
 // Given a string representation of a URL, sets up a connection and gets
 // an input stream.
    
    
    
    public void processForgetPassword(View view){
    	String email = emailText.getText().toString();
    	
    	String forgerPasswordURL = "http://demosafetyawarenessapps.safetyawarenessapps.com/app/index.php/user/feed?email="+email+"&sendpassword=1&apptoken=uscsa1221$&create=0";
    	if(com.haveNetworkConnection(this)){
    	try
        {
                //create an instance of the DefaultHandler class 
                //**ALTER THIS FOR YOUR CLASS NAME**
            final XMLDataHandler handler = new XMLDataHandler();
                //get the string list by calling the public method
            
            NetworkResponse responseDelegate = new NetworkResponse() {
				
				public void updateNetworkTask(String value, ArrayList<ReportData> reportDataArray, Context context, String fact) {
					// TODO Auto-generated method stub
					System.out.println(value);
					String statusValue = "";
					Common common = new Common();
					if(value.equalsIgnoreCase("OK")){
		            	statusValue = "Please check your email, Password has sent";
		            	common.showPopup(statusValue,context);
		            }else if(value.equalsIgnoreCase("FAILED BAD EMAIL")){
		            	statusValue = "The email you entered was not found in our system";
		            	common.showPopup(statusValue,context);
		            	 emailText.setText("");
		            	 passwordText.setText("");
		            }else{
		            	statusValue = "Please enter your email address before pressing the FORGOT PASSWORD button.";
		            	common.showPopup(statusValue,context);
		            }
				}
			};
              
			handler.getData(forgerPasswordURL,this, responseDelegate);
            
        }
        catch(Exception pce) {
        	//Log.e("AndroidTestsActivity", "PCE "+pce.getMessage());
        }
    	}else{
    		com.showPopup(Constant.connectionMessage, this);
    	}
    }

}

