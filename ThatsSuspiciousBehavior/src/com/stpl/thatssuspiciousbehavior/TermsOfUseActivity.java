package com.stpl.thatssuspiciousbehavior;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.support.v4.app.NavUtils;

public class TermsOfUseActivity extends Activity {

	boolean customTitleSupported;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.activity_terms_of_use);
        customTitleBar(getText(R.string.title_activity_options).toString());
    }

    public void customTitleBar(String activityTitle) {
        if (customTitleSupported) {

            getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
                TextView titleActivity = (TextView) findViewById (R.id.centerText);
 
                titleActivity.setText(activityTitle);
 
                Button termsButton;
                termsButton = (Button) findViewById(R.id.terms_button);
                //hide the progress bar if it is not needed
                termsButton.setVisibility(Button.GONE);
        }
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_terms_of_use, menu);
        return true;
    }

    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
