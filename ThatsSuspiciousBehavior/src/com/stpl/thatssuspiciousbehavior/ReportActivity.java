package com.stpl.thatssuspiciousbehavior;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.stpl.thatssuspiciousbehavior.Models.Common;
import com.stpl.thatssuspiciousbehavior.Models.Constant;
import com.stpl.thatssuspiciousbehavior.Models.MyLocation;
import com.stpl.thatssuspiciousbehavior.Models.NetworkResponse;
import com.stpl.thatssuspiciousbehavior.Models.ReportData;

public class ReportActivity extends Activity {

	ProgressDialog progress; 
	boolean customTitleSupported;
	Common com;
	String reportedIncident = "";
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        com = new Common();
        customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.activity_report);
        customTitleBar(getText(R.string.title_activity_report).toString());
        
    }

    public void customTitleBar(String activityTitle) {
        if (customTitleSupported) {

            getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
                TextView titleActivity = (TextView) findViewById (R.id.centerText);
 
                titleActivity.setText(activityTitle);
                Button backButton = (Button) findViewById(R.id.back_button);
                //hide the button if it is not needed
                backButton.setVisibility(Button.GONE);
                
        }
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_report, menu);
        return false;
    }

    public void getShareMessageView(View view){
    	
    	// Restore preferences
        SharedPreferences settings = getSharedPreferences("EmailStorage", 0);
        String savedEmail = settings.getString("emailString", "");
        System.out.println("email in default"+savedEmail);
        
    	Button clickedReportButton = (Button) findViewById(view.getId());
    	int tag = Integer.parseInt(clickedReportButton.getTag().toString());
    	//int tag = clickedReportButton.getId();
    	String reportText="";
    	if(tag == 0){
    		reportText = "Aggressive Begging";
    	}else if(tag == 1){
    		reportText = "Vandal";
    	}else if(tag == 2){
    		reportText = "Creepy Person";
    	}else if(tag == 3){
    		reportText = "Fight";
    	}else if(tag == 4){
    		reportText = "Possible Theft";
    	}else{
    		
    	}
    
    	reportedIncident = reportText;
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	String currentDateAndTime = sdf.format(new Date());
    	/*&double longitude = com.getCurrentLocation(this).getLongitude();
    	String longiString = Double.toString(longitude);
    	double latitude = com.getCurrentLocation(this).getLongitude();
    	String latString = Double.toString(latitude);*/
    	MyLocation location = new MyLocation();
    	location = com.getCurrentLocation(this);
    	if((location.getLattitude() == 0)||(location.getLongitude() == 0)){
    		com.showPopup("Can Not Find Your Location", this);
    		return;
    	}
    	String makingReportURL = "http://demosafetyawarenessapps.safetyawarenessapps.com/app/index.php/activity/feed?type="+reportText+"&latitude="+com.getCurrentLocation(this).getLattitude()+"&longitude="+com.getCurrentLocation(this).getLongitude()+"&date_time="+currentDateAndTime+"&email="+savedEmail+"&apptoken=uscsa1221$&create=1";
    	//String makingReportURL = "http://demosafetyawarenessapps.safetyawarenessapps.com/app/index.php/activity/feed?type="+reportText+"&latitude="+latString+"&longitude="+longiString+"&date_time="+currentDateAndTime+"&email="+savedEmail+"&apptoken=uscsa1221$&create=1";
    	
    	
    	// Hard code url for making report api
    	//String makingReportURL = "http://demosafetyawarenessapps.safetyawarenessapps.com/app/index.php/activity/feed?type=Aggressive%20Begging&latitude=37.785835&longitude=-122.406418&date_time=2012-08-31%2015:47:47&email=shweta.indorian@gmail.com&apptoken=uscsa1221$&create=1";
    	System.out.println("makingReportURL"+makingReportURL);
    	if(com.haveNetworkConnection(this)){
        	
    	try
        {
                //create an instance of the DefaultHandler class 
                //**ALTER THIS FOR YOUR CLASS NAME**
            XMLDataHandler handler = new XMLDataHandler();
            
            NetworkResponse responseDelegate = new NetworkResponse() {
				
				public void updateNetworkTask(String value, ArrayList<ReportData> reportDataArray, Context context, String fact) {
					// TODO Auto-generated method stub
					System.out.println(value);

		            if(value.equalsIgnoreCase("OK")){
		            	Intent intent = new Intent(context, ReportShareActivity.class);
		            	intent.putExtra("report type", reportedIncident);
		            	intent.putExtra("fact", fact);
		                startActivity(intent);
		            }else{
		            	 com.showPopup(value,context);
		            }
				}
			};
                
			handler.getData(makingReportURL,this, responseDelegate);
            
        }
        catch(Exception exception) {
        	//Log.e("AndroidTestsActivity", "exception "+exception.getMessage());
        }
    	}else{
    		com.showPopup(Constant.connectionMessage, this);
    	}
    	
    	
    	
    }
    
    
    public void getOptionViews(View view) {
    	Intent intent = new Intent(this, OptionsActivity.class);
        startActivity(intent);
    }
    
    public void getTermsActivity(View view){
    	Intent intent = new Intent(this, WebViewActivity.class);
        intent.putExtra(Common.URL_KEY, "http://demosafetyawarenessapps.safetyawarenessapps.com/plist/terms.html");
        intent.putExtra("title","TERMS");
        startActivity(intent);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    public void callingOnNumber(View view){
    	com.callingProcess(this);
    }
    
    
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK ) {
        	Intent homeIntent = new Intent(this,MainActivity.class);
            startActivity(homeIntent);
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}
