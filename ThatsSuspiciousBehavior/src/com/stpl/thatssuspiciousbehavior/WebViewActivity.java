package com.stpl.thatssuspiciousbehavior;



import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.stpl.thatssuspiciousbehavior.Models.Common;
import com.stpl.thatssuspiciousbehavior.Models.Constant;

public class WebViewActivity extends Activity {
    private WebView webView;
    boolean customTitleSupported;
    Common com;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        com = new Common();
        
        customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.activity_web_view);
        
        //Get Extra from Intents
        Bundle extras = getIntent().getExtras();
        String urlValue = extras.getString(Common.URL_KEY); 
        String title = extras.getString("title");
        
        customTitleBar(title);
        
        
        //Load web view from URL
        if(com.haveNetworkConnection(this)){
        webView = (WebView) findViewById(R.id.webViewTSB);
        webView.loadUrl(urlValue);
        }else{
        	com.showPopup(Constant.connectionMessage, this);
        }
        //webView.getSettings().setJavaScriptEnabled(true);
        //webView.loadUrl("http://demosafetyawarenessapps.safetyawarenessapps.com/plist/aboutus.html");
        //String customHtml = "<html><body><h1>Hello, WebView</h1></body></html>";
        //webView.loadData(customHtml, "text/html", "UTF-8");  
    }
    
    public void customTitleBar(String activityTitle) {
        if (customTitleSupported) {

            getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
                TextView titleActivity = (TextView) findViewById (R.id.centerText);
 
                titleActivity.setText(activityTitle);
 
                Button termsButton;
                termsButton = (Button) findViewById(R.id.terms_button);
                //hide the progress bar if it is not needed
                termsButton.setVisibility(Button.GONE);
                Button backButton = (Button) findViewById(R.id.back_button);
                //hide the button if it is not needed
                backButton.setVisibility(Button.GONE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_web_view, menu);
        return false;
    }
}
