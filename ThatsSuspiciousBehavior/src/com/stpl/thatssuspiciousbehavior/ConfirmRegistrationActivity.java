package com.stpl.thatssuspiciousbehavior;



import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.stpl.thatssuspiciousbehavior.Models.Common;
import com.stpl.thatssuspiciousbehavior.Models.Constant;
import com.stpl.thatssuspiciousbehavior.Models.NetworkResponse;
import com.stpl.thatssuspiciousbehavior.Models.ReportData;

public class ConfirmRegistrationActivity extends Activity {


    boolean customTitleSupported;
    EditText verificationText;
    String email, password;
    String signUPURL = "";
    Common com;
    ProgressDialog progress; 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      //vj to avoid keyboard appearance automatically
        //this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN); 
        //vj
        com = new Common();
        customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.activity_confirm_registration);
        customTitleBar(getText(R.string.title_activity_confirm_registration).toString());
        verificationText = (EditText) findViewById(R.id.verification_code_text);
        //Get Extra from Intents
        Bundle extras = getIntent().getExtras();
        email = extras.getString("email");
        password = extras.getString("password");
        signUPURL = extras.getString("signUPURL");
    }

    public void customTitleBar(String activityTitle) {
        if (customTitleSupported) {

            getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
                TextView titleActivity = (TextView) findViewById (R.id.centerText);
 
                titleActivity.setText(activityTitle);
 
                Button termsButton;
                Button backButton;
                termsButton = (Button) findViewById(R.id.terms_button);
                backButton = (Button) findViewById(R.id.back_button);
                //hide the progress bar if it is not needed
                termsButton.setVisibility(Button.GONE);
                /*termsButton.setBackgroundResource(R.drawable.button_send);
                termsButton.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                    	ConfirmRegistrationActivity confirmActivity = new ConfirmRegistrationActivity();
                    	confirmActivity.verifyCode(v);
                        }
                    });*/
                backButton.setVisibility(Button.GONE);
        }
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_confirm_registration, menu);
        return false;
    }

    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*public void verifyCode(View view){
    	String verificationCode = verificationText.getText().toString();
    	if((verificationCode != null) && (verificationCode != "")){
    		
    		String confirmationURL = "http://demosafetyawarenessapps.safetyawarenessapps.com/app/index.php/user/feed?email="+email+"&password="+password+"&activation=123456&create=0&apptoken=uscsa1221$";
        	
        	try
            {
                    //create an instance of the DefaultHandler class 
                    //**ALTER THIS FOR YOUR CLASS NAME**
                XMLDataHandler handler = new XMLDataHandler();
                    //get the string list by calling the public method
                String statusValue  = handler.getData(confirmationURL);
                System.out.println("statusValue"+statusValue);
                if(statusValue.equalsIgnoreCase("OK")){
                	
                	 Intent intent = new Intent(this, ReportActivity.class);
                     startActivity(intent);
                }else{
                	 showPopup(statusValue);
                }
            }
            catch(Exception exception) {
            	//Log.e("AndroidTestsActivity", "exception "+exception.getMessage());
            }
    		
    	}
    	else{
    		//show pop up for enter code
    	}
    }*/
    
    
   // public void verifyCode(View view){
    public void verifyCode(View view){
    	String verificationCode = verificationText.getText().toString();
    	if((verificationCode != null) && (verificationCode != "")){
    	    		
    		String confirmationURL = "http://demosafetyawarenessapps.safetyawarenessapps.com/app/index.php/user/feed?email="+email+"&password="+password+"&activation="+verificationCode+"&create=0&apptoken=uscsa1221$";
    		if(com.haveNetworkConnection(this)){
    			try
    	        {
    	                //create an instance of the DefaultHandler class 
    	                //**ALTER THIS FOR YOUR CLASS NAME**
    	            XMLDataHandler handler = new XMLDataHandler();
    	            String statusValue = "";
    	            NetworkResponse responseDelegate = new NetworkResponse() {
    					
    					public void updateNetworkTask(String value,ArrayList<ReportData> reportDataArray, Context context, String fact) {
    						// TODO Auto-generated method stub
    						System.out.println(value);
    						if(value.equalsIgnoreCase("OK")){
    		                	
    		                	// We need an Editor object to make preference changes.
    		                    // All objects are from android.context.Context
    		                    SharedPreferences settings = getSharedPreferences("EmailStorage", 0);
    		                    SharedPreferences.Editor editor = settings.edit();
    		                    editor.putString("emailString", email);

    		                    // Commit the edits!
    		                    editor.commit();
    		                	 Intent intent = new Intent(context, ReportActivity.class);
    		                     startActivity(intent);
    		                }else{
    		                	 com.showPopup(value,context);
    		                }
    						
    					}
    				};
    	                //get the string list by calling the public method
    	            //String statusValue  = handler.getData(loginURL,this);
    	            handler.getData(confirmationURL,this, responseDelegate);
    	            
    	            System.out.println("statusValue"+statusValue);
    	            
    	        }
    	        catch(Exception exception) {
    	        	//Log.e("AndroidTestsActivity", "exception "+exception.getMessage());
    	        }
    		}else{
    			com.showPopup(Constant.connectionMessage, this);
    		}
    	}
    	else{
    		
    		com.showPopup("Please enter verification code",this);
    		
    	}
    	    	
    }
    
    public void resendVerificationCode(View view){
    	
    	if(com.haveNetworkConnection(this)){
        try
        {
                //create an instance of the DefaultHandler class 
                //**ALTER THIS FOR YOUR CLASS NAME**
            XMLDataHandler handler = new XMLDataHandler();
                //get the string list by calling the public method
    		String resendURL = "http://demosafetyawarenessapps.safetyawarenessapps.com/app/index.php/user/feed?email="+email+"&password="+password+"&activation=0"+"&create=0&apptoken=uscsa1221$";

    		 NetworkResponse responseDelegate = new NetworkResponse() {
 				
 				public void updateNetworkTask(String value, ArrayList<ReportData> reportDataArray, Context context, String fact) {
 					// TODO Auto-generated method stub
 					System.out.println(value);
 					
 					if(value.equalsIgnoreCase("OK")){
 		            	Intent intent = new Intent(context, ConfirmRegistrationActivity.class);
 		            	intent.putExtra("email",email);
 		            	intent.putExtra("password", password);
 		            	intent.putExtra("signUPURL", signUPURL);
 		            	intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
 		                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
 		                startActivity(intent);
 		            }else{
 		            	 com.showPopup(value, context);
 		            }
 				}
 			};
                 //get the string list by calling the public method
             //String statusValue  = handler.getData(loginURL,this);
             handler.getData(resendURL,this, responseDelegate);
    		
             
        }
        catch(Exception pce) {
        	//Log.e("AndroidTestsActivity", "PCE "+pce.getMessage());
        }
    	}else{
    		com.showPopup(Constant.connectionMessage, this);
    	}
    	
    }
   
}
