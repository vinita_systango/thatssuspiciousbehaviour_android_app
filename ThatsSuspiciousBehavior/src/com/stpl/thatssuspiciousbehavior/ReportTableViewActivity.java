package com.stpl.thatssuspiciousbehavior;

import com.stpl.thatssuspiciousbehavior.Models.Common;
import com.stpl.thatssuspiciousbehavior.Models.Constant;
import com.stpl.thatssuspiciousbehavior.Models.MyLocation;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class ReportTableViewActivity extends Activity {

	MyLocation loc;
	Common com;
	ReportMapActivity reportMapActivity;
	boolean customTitleSupported;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        com = new Common();
        customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.activity_report_table_view);
        customTitleBar(getText(R.string.title_activity_login).toString());
        loc = new MyLocation();
        reportMapActivity = new ReportMapActivity();
        com = new Common();
    }

    public void customTitleBar(String activityTitle) {
        if (customTitleSupported) {

            getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
                TextView titleActivity = (TextView) findViewById (R.id.centerText);
 
                titleActivity.setText(activityTitle);
                Button backButton = (Button) findViewById(R.id.back_button);
                //hide the button if it is not needed
                backButton.setVisibility(Button.GONE);
                
        }
    }
    
    public void getReportSpecificMap(View view){
    	Button clickedReportButton = (Button) findViewById(view.getId());
    	int tag = Integer.parseInt(clickedReportButton.getTag().toString());
    	//int tag = clickedReportButton.getId();
    	String reportText="";
    	if(tag == 0){
    		reportText = "Aggressive Begging";
    	}else if(tag == 1){
    		reportText = "Vandal";
    	}else if(tag == 2){
    		reportText = "Creepy Person";
    	}else if(tag == 3){
    		reportText = "Fight";
    	}else if(tag == 4){
    		reportText = "Possible Theft";
    	}else{
    		
    	}
    	String finalURL = Constant.serverUrl+"latitude="+loc.getLattitude()+"&longitude="+loc.getLongitude()+"&incident_area=3&type="+reportText;
    	String timeFrameURL = com.makeReportUrl(Constant.reports_by_incident,finalURL,7);
        reportMapActivity.getReportResponse(timeFrameURL);	
        finish();
    }
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_report_table_view, menu);
        return false;
    }
    
    public void getOptionViews(View view) {
    	Intent intent = new Intent(this, OptionsActivity.class);
        startActivity(intent);
    }
    
    public void getTermsActivity(View view){
    	 Intent intent = new Intent(this, TermsOfUseActivity.class);
         intent.putExtra(Common.URL_KEY, "http://demosafetyawarenessapps.safetyawarenessapps.com/plist/terms.html");
         startActivity(intent);
    }
   
    public void getReportMapView(View view){
    	reportMapActivity.setMyPosition(view);
    	finish();
    }
    
    public void setMyPosition(View view){
    	Intent intent = new Intent(this, ReportMapActivity.class);
        startActivity(intent);
    }
}
