
package com.stpl.thatssuspiciousbehavior.Models;

public class Constant {

	//public static final String serverUrl = "http://safetyawarenessapps.com/app/index.php";
	public static final String serverUrl = "http://demosafetyawarenessapps.safetyawarenessapps.com/app/index.php/activity/feed?";
	// For Fetching Report's URL
	public static final int reports_by_default = 0;
	public static final int reports_by_incident = 1; 
	public static final int reports_by_time_range = 2;
	public static final String connectionMessage = "Connection unavailable";
	public Constant() {
		// TODO Auto-generated constructor stub
	}

}
