package com.stpl.thatssuspiciousbehavior.Models;

public class MyLocation {

	private double longitude;
	private double latitude;
	
	public MyLocation() {
		// TODO Auto-generated constructor stub
		
		
	}
	
	public MyLocation(double lati, double longi) {
		// TODO Auto-generated constructor stub
		longitude = longi;
		latitude = lati;
		
	}
    
	public void setLongitude(double longi){
		longitude = longi;
	}
	public void setLatitude(double lat){
		latitude = lat;
	}
	
	public double getLongitude(){
		return longitude;
	}
	
	public double getLattitude(){
		return latitude;
	}
}
