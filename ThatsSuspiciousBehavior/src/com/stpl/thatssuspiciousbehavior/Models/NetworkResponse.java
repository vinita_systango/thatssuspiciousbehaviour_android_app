package com.stpl.thatssuspiciousbehavior.Models;

import java.util.ArrayList;

import android.content.Context;

public interface NetworkResponse {

	public void updateNetworkTask(String str,ArrayList<ReportData> reportDataArray,Context context, String fact);
	
}