package com.stpl.thatssuspiciousbehavior.Models;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class FetchTask extends AsyncTask<Object, Object, Object> {

	 ProgressDialog progress;
	 Context context;
	 
	 
	    public FetchTask(Context cont){
	    	context = cont;
	    }
	    @Override
	    protected void onPreExecute() 
	    {
	         super.onPreExecute();
	         progress=ProgressDialog.show(context, "", "Loading...");
	    }

	    @Override
	    protected Object doInBackground(Object... params) 
	    {

	        // fetch your data here 

	         return null;
	    }

	    @Override
	    protected void onPostExecute(Object result) 
	    {
	        super.onPostExecute(result);
	        progress.dismiss();
	    }
	
}
