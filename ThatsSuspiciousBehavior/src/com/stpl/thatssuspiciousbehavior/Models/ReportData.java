package com.stpl.thatssuspiciousbehavior.Models;

public class ReportData {

	private MyLocation myLocation;
	private String reportType;
	private String reportDate;
	private String reportedBy;
	
	public ReportData() {
		// TO DO
	}
	
	public void setLocation(MyLocation loc){
		myLocation = loc;
	}
	
	public void setReportType(String type){
		reportType = type;
	}
	
	public void setReportDate(String date){
		reportDate = date;
	}
	
	public void setReportedBy(String by){
		reportedBy = by;
	}
	
	
	
	public MyLocation getLocation(){
		return myLocation;
	}
	
	public String getReportType(){
		return reportType;
	}
	
	public String getReportDate(){
		return reportDate;
	}
	
	public String getReportedBy(){
		return reportedBy;
	}

}
