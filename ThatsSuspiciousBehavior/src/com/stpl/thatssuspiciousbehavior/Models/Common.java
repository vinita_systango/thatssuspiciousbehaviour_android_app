package com.stpl.thatssuspiciousbehavior.Models;

import java.net.URL;

import org.xml.sax.InputSource;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.maps.GeoPoint;



	public class Common  {
	    public static final String URL_KEY = "URL_KEY" ;
	    GeoPoint geoPoint;
	    LocationManager locManager;
	    double latitude, longitude;
	    MyLocation myLocation = null;
	    // To check the Internet Connection
	   public boolean haveNetworkConnection(Context context) {
	        boolean haveConnectedWifi = false;
	        boolean haveConnectedMobile = false;

	        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
	        for (NetworkInfo ni : netInfo) {
	            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
	                if (ni.isConnected())
	                    haveConnectedWifi = true;
	            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
	                if (ni.isConnected())
	                    haveConnectedMobile = true;
	        }
	        return haveConnectedWifi || haveConnectedMobile;
	    }
	    
	   public MyLocation getCurrentLocation(Context context){
		   
		   Location location;
	        locManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE); 
	        //locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,1000L,500.0f, locationListener);
	       // Location location = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
	        if(locManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
	        		{
	        	locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,1000L,500.0f, locationListener);
	        	location = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
	        	
	        		}else{
	        			locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
	                	location = locManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
	                	//enableLocationSettings();
	        		}
	        
	        /*final boolean gpsEnabled = locManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

	        if (!gpsEnabled) {
	            // Build an alert dialog here that requests that the user enable
	            // the location services, then when the user clicks the "OK" button,
	            enableLocationSettings();
	        }*/
	        
	        if(location != null)                                
	        {
	            latitude = location.getLatitude();
	            longitude = location.getLongitude();
	        }else{
	        	//this.showPopup("Can Not Find Your Location", context);
	        	//latitude = 22.690085;
	        	//longitude = 75.892320;
	        }
	        myLocation = new MyLocation(latitude,longitude);
	        return myLocation;
	   }
	   
	   
	   private final LocationListener locationListener = new LocationListener() {
	        public void onLocationChanged(Location location) {
	            //updateWithNewLocation(location);
	        	latitude = location.getLatitude();
	        	longitude = location.getLongitude();
	        	myLocation = new MyLocation(latitude, longitude);
	        }

	        public void onProviderDisabled(String provider) {
	            //updateWithNewLocation(null);
	        }

	        public void onProviderEnabled(String provider) {
	        }

	        public void onStatusChanged(String provider, int status, Bundle extras) {
	        }
	    };
	    
	    
	    
	    /*private void enableLocationSettings() {
	        Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
	        startActivity(settingsIntent);
	    }*/
	    
	    
	
	   public String makeReportUrl(int requestType, String url, float dateRange){
	    String preparedURL = "";
	    
	    if(dateRange == 0){
	    	dateRange = 7;
	    }
	    switch(requestType){
	    
	    case Constant.reports_by_default:
	    	preparedURL = url + "&incident_area=3&daterange="+dateRange+"&create=0&apptoken=uscsa1221$";
	    	break;
	    
	    case Constant.reports_by_incident:
	    	preparedURL = url + "&daterange="+dateRange+"&create=0&apptoken=uscsa1221$";
	    	break;
	    case Constant.reports_by_time_range:	
	    	preparedURL = url + "&incident_area=3&daterange="+dateRange+"&create=0&apptoken=uscsa1221$";
	    	break;
	    
	    default:
	    	
	    }
	    	return preparedURL;
	    }
	    
	    
	    public void showPopup(String message, Context context){
	    	// prepare the alert box                
	        AlertDialog.Builder alertbox = new AlertDialog.Builder(context);
	        // set the message to display
	        alertbox.setMessage(message); 
	        // add a neutral button to the alert box and assign a click listener
	        alertbox.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
	             
	            // click listener on the alert box
	            public void onClick(DialogInterface arg0, int arg1) {
	                // the button was clicked
	                
	            }
	        });
	         
	        // show it
	        alertbox.show();
	    }
	    
	   public void callingProcess(final Context context){
		   AlertDialog.Builder builder = new AlertDialog.Builder(context);
	    	builder.setMessage("Do you wish to call 911?")
	    	       .setCancelable(false)
	    	       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
	    	           public void onClick(DialogInterface dialog, int id) {
	    	        	   try{
	    	               	Intent callIntent = new Intent(Intent.ACTION_CALL);
	    	               	callIntent.setData(Uri.parse("tel:911"));
	    	               	context.startActivity(callIntent);
	    	               	}
	    	           	catch (Exception e) {
	    	                   e.printStackTrace();
	    	                 }
	    	           }
	    	       })
	    	       .setNegativeButton("No", new DialogInterface.OnClickListener() {
	    	           public void onClick(DialogInterface dialog, int id) {
	    	                dialog.cancel();
	    	           }
	    	       });
	    	
	    	AlertDialog alert = builder.create();
	    	alert.show();
	   }
	    
	   
	   
	  public static boolean checkHostConnection(){
		 
		  try{
		  URL xmlURL= new URL("http://demosafetyawarenessapps.safetyawarenessapps.com/app/index.php".replace(" ", "%20"));
          //parse the data
           InputSource inputSource  =  new InputSource(xmlURL.openStream());
		  }
		  catch(Exception ex){
			  return false;
		  }
		  return true;
	  }
	}


