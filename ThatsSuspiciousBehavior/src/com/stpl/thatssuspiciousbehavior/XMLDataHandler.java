package com.stpl.thatssuspiciousbehavior;



import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.stpl.thatssuspiciousbehavior.Models.Common;
import com.stpl.thatssuspiciousbehavior.Models.FetchTask;
import com.stpl.thatssuspiciousbehavior.Models.MyLocation;
import com.stpl.thatssuspiciousbehavior.Models.NetworkResponse;
import com.stpl.thatssuspiciousbehavior.Models.ReportData;

public class XMLDataHandler extends DefaultHandler {

	//list for imported product data
	private String elementValue = "";
	//string to track each entry
	public String statusString = "";
	public String factValue = "";
	//flag to keep track of XML processing
	private boolean isStatus = false;
	private boolean isReport = false;
	//String latitude = "";
	//String longitude = "";
	MyLocation loc = null;
	ReportData reportData = null;
	ArrayList<ReportData> reportDataArray ;
	
	//constructor
	public XMLDataHandler() {
	// TODO Auto-generated constructor stub
	super();
	elementValue = "";
	}
	
	

	//opening element tag
	public void startElement (String uri, String localName, String qName, Attributes attributes) throws SAXException 
	{
	    //handle the start of an element
	if(localName.equals("status"))
	{
	 	//elementValue = "";
	    isStatus = true;
	}
	else if(localName.equals("reports")){

	 reportDataArray = new ArrayList<ReportData>();
	}
	else if(localName.equals("report")){
	isReport = true;
	reportData = new ReportData();
	loc = new MyLocation();
	// Get the number of attribute
	        int length = attributes.getLength();

	        // Process each attribute
	        for (int i=0; i<length; i++) {
	            // Get names and values for each attribute
	            String name = attributes.getQName(i);
	            String value = attributes.getValue(i);
	            if(name.equals("type")){
	            	reportData.setReportType(value);
	            }
	}
	
	}	
	else if(localName.equals("date")){
	
	}
	else if(localName.equals("latitude")){
	
	}
	else if(localName.equals("longitude")){
	
	}
	else if(localName.equals("reportedby")){
	
	}
	else if(localName.equals("fact")){
		factValue = "";
	}
	}

	//closing element tag
	public void endElement (String uri, String localName, String qName) throws SAXException
	{
	    //handle the end of an element
	if(localName.equalsIgnoreCase("status"))
	{
	statusString = elementValue;
	elementValue = "";
	    isStatus = false;
	}
	else if(localName.equals("reports")){
	System.out.println("count"+reportDataArray.size());
	}
	else if(localName.equals("report")){
	reportData.setLocation(loc);
	reportDataArray.add(reportData);
	isReport = false;
	//reportData = null;
	//loc = null;
	}
	else if(localName.equals("date")){
	reportData.setReportDate(elementValue);
	}
	else if(localName.equals("latitude")){
	loc.setLatitude(Double.parseDouble(elementValue));
	}
	else if(localName.equals("longitude")){
	loc.setLongitude(Double.parseDouble(elementValue));
	}
	else if(localName.equals("reportedby")){
	reportData.setReportedBy(elementValue);
	}
	else if(localName.equals("fact")){
		factValue = elementValue;
	}
}

	//element content
	public void characters (char ch[], int start, int length) throws SAXException
	{
	    //process the element content
	//string to store the character content
	String currText = "";
	//loop through the character array
	
	for (int i=start; i<start+length; i++)
	{
	    switch (ch[i]) {
	    case '\\':
	        break;
	    case '"':
	        break;
	    case '\n':
	        break;
	    case '\r':
	        break;
	    case '\t':
	        break;
	    default:
	        currText += ch[i];
	        break;
	    }
	}

        elementValue  = currText;
	//prepare for the next item
	if (isStatus) {
            isStatus = false;
        }
	else if(isReport){
	//isReport = false;
	}
	
	//if(isStatus && currText.length()>0)
	  //  statusString += currText+"\n";
	}

	public void getData(String url, Context context, NetworkResponse response)
	{
	//new FetchTask(context).execute();
		Common com = new Common();
		//ProgressDialog progress=ProgressDialog.show(context, "", "Loading...");
		if(Common.checkHostConnection()){	
			new NetworkTask(context, response).execute(url);
			}else{
				com.showPopup("Unknown Host", context);
			}
		//progress.dismiss();
	//take care of SAX, input and parsing errors
	    /*try
	    {
	            //set the parsing driver
	        System.setProperty("org.xml.sax.driver","org.xmlpull.v1.sax2.Driver");
	            //create a parser
	        SAXParserFactory parseFactory = SAXParserFactory.newInstance();
	        SAXParser xmlParser = parseFactory.newSAXParser();
	            //get an XML reader
	        XMLReader xmlIn = xmlParser.getXMLReader();
	            //instruct the app to use this object as the handler
	        
	        xmlIn.setContentHandler(this);
	            //provide the name and location of the XML file **ALTER THIS FOR YOUR FILE**
	        
	       
	        URL xmlURL= new URL(url.toString().replace(" ", "%20"));
	      //parse the data
	        xmlIn.parse(new InputSource(xmlURL.openStream()));
	            //open the connection and get an input stream
	        //URLConnection xmlConn = xmlURL.openConnection();
	        
	        
	        //InputStreamReader xmlStream = new InputStreamReader(xmlConn.getInputStream());
	        //BufferedReader xmlBuff = new BufferedReader(xmlStream);
	      // String stringForInputStream = convertStreamToString(xmlConn.getInputStream());	
	       //System.out.println("xmlString:"+stringForInputStream);
	            //build a buffered reader
	        //String xmlString = getStringFromBuffer(xmlBuff);
	        
	    }
	    
	    catch(SAXException se) { 
	    	Log.e("AndroidTestsActivity", "SAX Error " + se.getMessage()); }
	    catch(IOException ie) {
	    	Log.e("AndroidTestsActivity","Input Error " + ie.getMessage()); }
	    catch(Exception oe) {
	    	Log.e("AndroidTestsActivity","Unspecified Error " + oe.getMessage()); }*/
	        //return the parsed product list
	    //return statusString;
	}
	
	
	// convert input stream into string
	/*public static String  convertStreamToString(InputStream  is) throws Exception  {
	    BufferedReader  reader = new BufferedReader (new InputStreamReader (is));
	    StringBuilder sb = new StringBuilder();
	    String  line = null;

	    while ((line = reader.readLine()) != null) {
	        sb.append(line);
	    }

	    //is.close();

	    return sb.toString();
	}*/
	
	/*public String getStringFromBuffer(BufferedReader buf) throws IOException{
	BufferedReader bRead = new BufferedReader(buf);
	String line = null;
	StringBuffer theText = new StringBuffer();
	while((line=bRead.readLine())!=null){
	   theText.append(line+"\n");
	}

	return theText.toString();
	}*/

	
	
	private class NetworkTask extends AsyncTask<String, Void, Object> {

	 ProgressDialog progress;
	 Context context;
	 NetworkResponse delegate;
	 XMLDataHandler handler;
	 
	public NetworkTask(Context cont, NetworkResponse responseDelegate) {
	context = cont;
	delegate = responseDelegate;
	// TODO Auto-generated constructor stub
	} 
	 
        /** The system calls this to perform work in a worker thread and
      * delivers it the parameters given to AsyncTask.execute() 
         * @return */
    protected Void doInBackground(String... urls) {
        InputSource inputSource = null ;
        try{
            URL xmlURL= new URL(urls[0].toString().replace(" ", "%20"));
              //parse the data
               inputSource  =  new InputSource(xmlURL.openStream());
               try
               {
                       //set the parsing driver
                   System.setProperty("org.xml.sax.driver","org.xmlpull.v1.sax2.Driver");
                       //create a parser
                   SAXParserFactory parseFactory = SAXParserFactory.newInstance();
                   SAXParser xmlParser = parseFactory.newSAXParser();
                       //get an XML reader
                   XMLReader xmlIn = xmlParser.getXMLReader();
                       //instruct the app to use this object as the handler
                   handler = new XMLDataHandler();
                   xmlIn.setContentHandler(handler);
                       //provide the name and location of the XML file **ALTER

                   xmlIn.parse(inputSource);
                   System.out.println(handler.statusString);
                  // progress.dismiss();
                   //progress.cancel();
                   
               }
               catch(SAXException se) {
                   Log.e("AndroidTestsActivity", "SAX Error " + se.getMessage()); }
               catch(IOException ie) {
                   Log.e("AndroidTestsActivity","Input Error " + ie.getMessage()); }

               catch(Exception oe) {
                   Log.e("AndroidTestsActivity","Unspecified Error " + oe.getMessage()); }
               
    }catch(Exception ex){
    }

    return null;

    }
    /** The system calls this to perform work in the UI thread and delivers
      * the result from doInBackground() */
    protected void onPostExecute(Object result) {
    	super.onPostExecute(result);
    	delegate.updateNetworkTask(handler.statusString, handler.reportDataArray, context, handler.factValue);
        progress.dismiss();
    }
    protected void onPreExecute() 
    {
         super.onPreExecute();
         progress=ProgressDialog.show(context, "", "Loading...");
         //progress = new ProgressDialog(context,R.style.popupStyle);
         //progress.show();
    }
}
	 
}
