
package com.stpl.thatssuspiciousbehavior;



import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.stpl.thatssuspiciousbehavior.Models.Common;

public class ReportShareActivity extends Activity {

	boolean customTitleSupported;
	Common com;
	String reportedIncident = "";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.activity_report_share);
        customTitleBar(getText(R.string.title_activity_report_share).toString());
        Bundle extras = getIntent().getExtras();
        reportedIncident = extras.getString("report type");
        String factValue = extras.getString("fact");
        System.out.println("length of fact string"+ factValue.length());
        TextView factView = (TextView) findViewById (R.id.fact_id); 
        factView.setText(factValue);
        com = new Common();
    }

    public void customTitleBar(String activityTitle) {
        if (customTitleSupported) {

            getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebar);
                TextView titleActivity = (TextView) findViewById (R.id.centerText);
 
                titleActivity.setText(activityTitle);
 
                Button termsButton;
                Button backButton;
                termsButton = (Button) findViewById(R.id.terms_button);
                backButton = (Button) findViewById(R.id.back_button);
                //hide the progress bar if it is not needed
                termsButton.setVisibility(Button.GONE);
                
                //change the back button background
               // backButton.setBackgroundResource(R.drawable.home_button);
                backButton.setVisibility(Button.GONE);
        }
    }
    
    /*public void shareReportOnFB(View view){
    	Intent sendIntent = null;
    	
    	
        String msg ="Something to share about";

        sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.setClassName("com.facebook.katana", "com.facebook.katana.ProfileTabHostActivity");
        sendIntent.setClassName("com.google.android.gm","com.google.android.gm.ComposeActivityGmail");
        sendIntent.setType("text/plain");


        sendIntent.putExtra(Intent.EXTRA_TEXT, msg);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.Terms_Of_Service_String_Second) + " \""
                + "Share title"+ "\" : " + getResources().getString(R.string.app_name));

        startActivity(Intent.createChooser(sendIntent, this.getString(R.string.About_String)));
        
    	Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
    	shareIntent.setType("text/plain");
    	shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Content to share");
    	PackageManager pm = v.getContext().getPackageManager();
    	List activityList = pm.queryIntentActivities(shareIntent, 0);
    	for (final ResolveInfo app : activityList) {
    	    if ((app.activityInfo.name).contains("facebook")) {
    	        final ActivityInfo activity = app.activityInfo;
    	        final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
    	        shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
    	        shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |             Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
    	        shareIntent.setComponent(name);
    	        v.getContext().startActivity(shareIntent);
    	        break;
    	   }
    	}}*/
        
        
    public void shareReportOnFB(View view){
    	
    	Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Suspicious Behavior Reported");
        intent.putExtra(Intent.EXTRA_TEXT,"I just reported "+reportedIncident+" incident at:"+ "https://maps.google.com/maps?q=loc:"+com.getCurrentLocation(this).getLattitude()+","+com.getCurrentLocation(this).getLongitude());
        startActivity(Intent.createChooser(intent, "Share with"));
    	
    	
    	
    	  //sendIntent.setClassName("com.facebook.katana", "com.facebook.katana.ProfileTabHostActivity");
        //sendIntent.setClassName("com.google.android.gm","com.google.android.gm.ComposeActivityGmail");
        
        //String mapLink = "<loc>:"+com.getCurrentLocation(this).getLattitude()+","+com.getCurrentLocation(this).getLongitude();
         //String linkTitle = "I just reported a " + "suspicious" +"incident on the USC Campus at: " ;
        //sendIntent.putExtra(Intent.EXTRA_HTML_TEXT, "<html><head><a href ="+mapLink+">"+linkTitle+"</a></head></html>");
        //sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Reported on TSB");

        
       /* Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
    	shareIntent.setType("text/plain");
    	shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Content to share");
    	PackageManager pm = v.getContext().getPackageManager();
    	List activityList = pm.queryIntentActivities(shareIntent, 0);
    	for (final ResolveInfo app : activityList) {
    	    if ((app.activityInfo.name).contains("facebook")) {
    	        final ActivityInfo activity = app.activityInfo;
    	        final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
    	        shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
    	        shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |             Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
    	        shareIntent.setComponent(name);
    	        v.getContext().startActivity(shareIntent);
    	        break;
    	   }
    	}*/
        
    }
    
    
    /* public void shareViaTwitter(View view){
	Intent shareIntent = findTwitterClient(); 
    shareIntent.putExtra(Intent.EXTRA_TEXT, "test");
    startActivity(Intent.createChooser(shareIntent, "Share"));
}


public Intent findTwitterClient() {
	final String[] twitterApps = {
	// package // name - nb installs (thousands)
	"com.twitter.android", // official - 10 000
	"com.twidroid", // twidroyd - 5 000
	"com.handmark.tweetcaster", // Tweecaster - 5 000
	"com.thedeck.android" };// TweetDeck - 5 000 };
	Intent tweetIntent = new Intent();
	tweetIntent.setType("text/plain");
	final PackageManager packageManager = getPackageManager();
	List<ResolveInfo> list = packageManager.queryIntentActivities(
	tweetIntent, PackageManager.MATCH_DEFAULT_ONLY);

	for (int i = 0; i < twitterApps.length; i++) {
	for (ResolveInfo resolveInfo : list) {
	String p = resolveInfo.activityInfo.packageName;
	if (p != null && p.startsWith(twitterApps[i])) {
	tweetIntent.setPackage(p);
	return tweetIntent;
	}
	}
	}
	return null;
}

public Intent findFBclient(){
	final String[] fbApps = {"com.facebook.katana"};
	Intent fbIntent = new Intent();
	fbIntent.setType("text/plain");
	final PackageManager packageManager = getPackageManager();
	List<ResolveInfo> list = packageManager.queryIntentActivities(
	fbIntent, PackageManager.MATCH_DEFAULT_ONLY);
	for (int i = 0; i < fbApps.length; i++) {
	for (ResolveInfo resolveInfo : list) {
	String p = resolveInfo.activityInfo.packageName;
	if (p != null && p.startsWith(fbApps[i])) {
	fbIntent.setPackage(p);
	return fbIntent;
	}
	}
	}
	return null;
}*/
    
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_report_share, menu);
        return false;
    }

    public void backButtonPressed(View view){
    	Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
    public void getOptionViews(View view) {
    	Intent intent = new Intent(this, OptionsActivity.class);
        startActivity(intent);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    public void callingOnNumber(View view){
    	com.callingProcess(this);
}

}
